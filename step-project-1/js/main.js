//Service Active Tab

$(".services-card").hide();
$(".services-card").eq(0).show();

$("#services-block-menu ul li").click( function(){
  let idx = $(this).index();
	$("#services-block-menu ul li").removeClass("services-link-active");
	$(this).toggleClass("services-link-active");
	$(".services-card").hide();
  $(".services-card").eq(idx).toggle(); 
});

// Load More Button

let loaded = false;

$('.btn-load').hide();
$('.list li').slice(12).hide();
$('.btn-load').click(() => {
  $('.list li').slice(12).show()
  $('.btn-load').hide();
  loaded = true;
});


// Filter - Our Amazing Work

$(document).ready(function(){
  $(".work-link").click(function(){
    let filterValue = $(this).attr("data-filter");
    let elem = $(".item");

    $(".work-link").removeClass("work-link-active");
    $(this).toggleClass("work-link-active");

    if(filterValue == "all"){  
      if(loaded === false) {
        $('.btn-load').show();
        $(elem).slice(0, 12).show(500);
        $(elem).slice(12).hide(500);
      } else {
        $('.btn-load').hide();
        $(elem).show(500);
      }
    }
    else{
      $('.btn-load').hide()
      $(elem).not("."+filterValue).hide(1000);
      $(elem).filter("."+filterValue).show(1000);
    }
  });
})

//SLIDER 

$('.slider-content').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: false,
  asNavFor: '.slider-nav'
});

$('.slider-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.slider-content',
  centerMode: true,
  centerPadding: '10px',
  focusOnSelect: true,
  arrows: true,
  nextArrow: '<i class="icon-angle-right"></i>',
  prevArrow:  '<i class="icon-angle-left"></i>',
  
});

  



		